import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TextPreprocessor;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by User: vivek.ke
 * Date: 7/23/15
 */
@RunWith(Parameterized.class)
public class TextPreprocessorNGramTest {
    private  String sentenceBoundary;
    private String actual;
    private String expected;

    public TextPreprocessorNGramTest(String sentenceBoundary, String actual, String expected) {
        this.sentenceBoundary = sentenceBoundary;
        this.actual= actual;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        Object[][] data = new Object[][]{
                {".", "actual1", "expected1"},
                {",", "actual2", "expected2"}
        };
        return Arrays.asList(data);
    }

    @Test
    public void replaceSpecialSymbolsWithPeriodSpace() {
        System.out.println("Tests for"+sentenceBoundary+actual+expected);
        assertEquals("convertNormalNGramBoundaries should replace " + sentenceBoundary + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries("String1" + sentenceBoundary + "String2"));
    }
}
