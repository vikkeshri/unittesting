import org.junit.Test;
import util.TextPreprocessor;

import static org.junit.Assert.assertEquals;

/**
 * Created by User: vivek.ke
 * Date: 7/22/15
 */
public class TextPreprocessorTest {

    @Test
    public void replaceSpecialSymbolsWithPeriodSpace() {

        assertEquals( "convertNormalNGramBoundaries should replace " + "."  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "." + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + ","  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "," + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + ":"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + ":" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + ";"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + ";" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "?"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "?" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "!"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "!" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "("  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "(" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + ")"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + ")" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "{"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "{" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "}"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "}" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "["  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "[" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "]"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "]" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "/"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "/" + "String2" ) );
        assertEquals( "convertNormalNGramBoundaries should replace " + "|"  + "with \".\" ", "String1. String2", TextPreprocessor.convertNormalNGramBoundaries( "String1" + "|" + "String2" ) );



    }

    @Test
    public void shouldRemoveTrailingSpaces() {
        assertEquals("convertMultipleSpaces should remove trailing space", "string", TextPreprocessor.convertMultipleSpaces("string "));
        assertEquals("convertMultipleSpaces should remove multiple trailing spaces", "string", TextPreprocessor.convertMultipleSpaces("string  "));
        assertEquals("convertMultipleSpaces should remove trailing tab", "string", TextPreprocessor.convertMultipleSpaces("string    "));
        assertEquals("convertMultipleSpaces should remove multiple trailing tabs", "string", TextPreprocessor.convertMultipleSpaces("string       "));
        assertEquals("convertMultipleSpaces should remove trailing end line", "string", TextPreprocessor.convertMultipleSpaces("string\n"));
        assertEquals("convertMultipleSpaces should remove multiple trailing end lines", "string", TextPreprocessor.convertMultipleSpaces("string\n\n"));
        assertEquals("convertMultipleSpaces should remove different trailing white characters", "string", TextPreprocessor.convertMultipleSpaces("string \n"));

    }

    @Test
    public void shouldRemoveSingleSpace() {
        assertEquals("convertMultipleSpaces should remove trailing space", "", TextPreprocessor.convertMultipleSpaces(" "));
        assertEquals("convertMultipleSpaces should remove trailing end line", "", TextPreprocessor.convertMultipleSpaces("\n"));
        assertEquals("convertMultipleSpaces should remove trailing tab", "", TextPreprocessor.convertMultipleSpaces("  "));
    }

    @Test
    public void shouldRemoveFrontSpaces() {
        assertEquals("convertMultipleSpaces should remove front space", "string", TextPreprocessor.convertMultipleSpaces(" string"));
        assertEquals("convertMultipleSpaces should remove multiple front spaces", "string", TextPreprocessor.convertMultipleSpaces("  string"));
        assertEquals("convertMultipleSpaces should remove front tab", "string", TextPreprocessor.convertMultipleSpaces("    string"));
        assertEquals("convertMultipleSpaces should remove multiple front tabs", "string", TextPreprocessor.convertMultipleSpaces("      string"));
        assertEquals("convertMultipleSpaces should remove front end line", "string", TextPreprocessor.convertMultipleSpaces("\nstring"));
        assertEquals("convertMultipleSpaces should remove multiple front end lines", "string", TextPreprocessor.convertMultipleSpaces("\n\nstring"));
    }

    @Test
    public void shouldRemoveMultipleSpaces() {
        assertEquals("convertMultipleSpaces should remove multiple spaces", "string abc", TextPreprocessor.convertMultipleSpaces("string  abc"));
        assertEquals("convertMultipleSpaces should remove multiple spaces", "string abc", TextPreprocessor.convertMultipleSpaces("string  \n abc"));
        assertEquals("convertMultipleSpaces should remove multiple spaces", "string abc", TextPreprocessor.convertMultipleSpaces("string    abc"));
    }

}
