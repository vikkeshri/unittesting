package util;

import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: jigar.p
 * Date: Feb 16, 2011
 * Time: 6:56:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeywordNormalizer {

    public static String normalize(String text){
        return normalize( text, false );
    }

    public static String normalize(String text, boolean excludeStopCharacters){
        text=text.toLowerCase();
        text=removeCharacters(text, excludeStopCharacters);
        text=convertCharsToSpace(text, excludeStopCharacters);
        text=replaceWords(text); // Already removed stop words
        text=convertMultipleSpaces(text);
        return text;
    }

    private static String removeCharacters(String text){
        return removeCharacters( text, false );
    }

    private static String removeCharacters(String text, boolean excludeSentenceBoundaries){
        if( excludeSentenceBoundaries )
            return text.replaceAll("\"|&|\\(|\\)|\\{|\\}|\\[|\\]|\\\\|\\/|:|;|(?i)'s|'|<|>|#","");
        return text.replaceAll("\"|&|\\(|\\)|\\{|\\}|\\[|\\]|,|\\?|\\\\|\\/|:|;|!|(?i)'s|'|<|>|#","");
    }

    private static String replaceWords(String text){
        String regexString="\\b(?i)(a|about|an|at|by|for|from|how|in|is|of|on|or|the|what|with)\\b";
        return text.replaceAll(regexString," ");
    }

    private static String convertCharsToSpace(String text){
        return convertCharsToSpace( text, false );
    }

    private static String convertCharsToSpace(String text, boolean excludeSentenceBoundaries){
        if( excludeSentenceBoundaries )
            return text.replaceAll("-|\\*|%|\\+"," ");
        return text.replaceAll("-|\\*|%|\\.|\\+"," ");
    }

    private static String convertMultipleSpaces(String text){
        return text.replaceAll("\\s{2,}", " ").trim();
    }

   public static String getBoiledTerm( String keywordTerm )
   {
        String[] keywordTermArray = keywordTerm.split( " " );
        StringBuffer boiledterm = new StringBuffer();
        Arrays.sort( keywordTermArray );
        for ( String keywordTermElement : keywordTermArray )
        {
            boiledterm.append( keywordTermElement );
        }
        return boiledterm.toString();
   }    

    public static void main(String[] args) {
        String text="\"A,!n[s-(h}u'%]L's or &\\Ban;s/?a:l\" at hOw?";
        System.out.println(normalize(text));
        text = "   My   name is khan   ";
        System.out.println(normalize(text));
        text = "वित्त प्रबंधन";
        System.out.println(normalize(text).replaceAll( " +", ""));
    }
}
