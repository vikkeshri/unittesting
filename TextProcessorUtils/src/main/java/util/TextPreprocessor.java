package util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by IntelliJ IDEA.
 * User: jigar.p
 * Date: Feb 16, 2011
 * Time: 6:56:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TextPreprocessor {

    private static Logger log = Logger.getLogger(TextPreprocessor.class.getName());
    private static HashSet<String> STOP_WORDS_LIST = null;
    private static HashSet<String> FILE_EXTENSION_LIST = null;
    private static HashSet<String> TLD_LIST = null;

    static {
        log.info("Loading stopword, tld and file extension lists...");
        /*STOP_WORDS_LIST = readListFromFile(ApplicationProperties.getProperty("STOPWORDS"));
        FILE_EXTENSION_LIST = readListFromFile(ApplicationProperties.getProperty("FILEEXTENSIONS"));
        TLD_LIST = readListFromFile(ApplicationProperties.getProperty("TLDS"));*/
        STOP_WORDS_LIST = new HashSet<>();
        FILE_EXTENSION_LIST = new HashSet<>();
        TLD_LIST = new HashSet<>();
    }

    private static HashSet<String> readListFromFile(String fileName) {
        String strLine;
        HashSet<String> resultList = new HashSet<String>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            while ((strLine = br.readLine()) != null) {
                resultList.add(strLine);
            }
            br.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Loaded " + fileName + "!!!");
        return resultList;
    }

    public static String processForTopicExtractor(String text) {
        long start = System.currentTimeMillis();
        text = text.toLowerCase();
        text = removeUrls(text);
        //Following operation is taking too long hence commented
        //text = removeTLDsAndFileExt(text);
        text = convertNormalNGramBoundaries(text);
        text = convertUnicodeNGramBoundaries(text);
        text = convertCharsToSpace(text);
//        text = removeStopWordsAndDigits(text);
        text = removeCharacters(text);
        text = removeStopWordsAndDigits(text);
        text = convertMultipleSpaces(text);
        long stop = System.currentTimeMillis();
        log.info("Text preprocessed in " + (stop - start) + " ms");
        return text;
    }

    public static String process(String text) {
        long start = System.currentTimeMillis();
        text = text.toLowerCase();
        text = removeUrls(text);
        //Following operation is taking too long hence commented
        //text = removeTLDsAndFileExt(text);
        text = convertNormalNGramBoundaries(text);
        text = convertUnicodeNGramBoundaries(text);
        text = convertCharsToSpace(text);
        text = removeStopWordsAndDigits(text);
        text = removeCharacters(text);
        text = removeStopWordsAndDigits(text);
        text = convertMultipleSpaces(text);
        long stop = System.currentTimeMillis();
        //log.info("Text preprocessed in " + ( stop - start ) + " ms");
        return text;
    }

    private static String removeUrls(String text) {
        text = text.replaceAll("(http|ftp|https)://[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-@?^=%&amp;/~\\+#])?", "");
        text = text.replaceAll("(www\\.)+.*?(\\s|$)+", "");
        return text;
    }

    private static String removeStopWordsAndDigits(String text) {
        String[] words = text.split("\\s");
        StringBuffer resultString = new StringBuffer();
        for (String word : words) {
            word = word.trim();
            String boiledWord = TextPreprocessor.removeNGramBoundaries(word).trim();
            boolean toRemove = false;
            if (word.matches(".*[0-9@].*") || STOP_WORDS_LIST.contains(word) || STOP_WORDS_LIST.contains(boiledWord)) {
                toRemove = true;
            }

            if (!toRemove) {
                resultString.append(" ").append(word);
            }
        }
        return resultString.toString();
    }

    private static String removeTLDsAndFileExt(String text) {
        String[] words = text.split("\\s");
        StringBuffer resultString = new StringBuffer();

        for (String word : words) {
            word = word.trim();
            boolean toRemove = false;
            for (String fileExt : FILE_EXTENSION_LIST) {
                if (word.matches(".*\\" + fileExt + "[\\.,;!\\?]?")) {
                    toRemove = true;
                    break;
                }
            }

            for (String tld : TLD_LIST) {
                if (word.matches(".*\\" + tld)) {
                    toRemove = true;
                    break;
                }
            }

            if (!toRemove) {
                resultString.append(" ").append(word);
            }
        }
        return resultString.toString();
    }

    private static String removeCharacters(String text) {
        return text.replaceAll("[\\\\#\\$%\\^\\*<>\\+=&~`\"\'®©™]", "");
    }

    private static String convertCharsToSpace(String text) {
        return text.replaceAll("[\\-_–—]", " ");
    }

    public static String convertNormalNGramBoundaries(String text) {
        return text.replaceAll("[\\.,:;\\?!\\(\\)\\{\\}\\[\\]/\\|\"]+", ". ");
    }

    public static String convertUnicodeNGramBoundaries(String text) {
        return text.replaceAll("[։܀܁܂၊။።፧፨᙮᠃᠉‼‽⁇⁈⁉。﹒﹗！．？｡;·،॥๚๛፡፣፤፥፦᙭᛫᛬᛭។៕៖៚᠂᠄᠅᠈᥄᥅、﹐﹑﹔﹕﹖，：；､]+", ". ");
    }

    public static String removeNGramBoundaries(String text) {
        return text.replaceAll("\\.", "").trim();
    }

    public static String convertMultipleSpaces(String text) {
        return text.replaceAll("\\s+", " ").trim();
    }

    public static void main(String[] args) {
    }

    public static String arrangeNGram(String keywordPhrase) {
        String[] resultArray = keywordPhrase.split(" ");
        String result = "";
        Arrays.sort(resultArray);
        String prev = null;
        for (String word : resultArray) {
            if (word.length() > 1 && !word.equals(prev)) {
                result += " " + word;
                prev = word;
            }
        }
        return result.trim();
    }
}
