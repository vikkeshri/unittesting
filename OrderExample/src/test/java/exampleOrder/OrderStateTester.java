package exampleOrder;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by vivek.ke
 */
public class OrderStateTester {
    private static String warehouse1 = "warehouse1";
    private static String warehouse2 = "warehouse2";
    private WareHouse wareHouse;
    private static Double epsilon = Math.pow(10, -9);

    @Before
    public void setUp() {
        // setup phase
        HashMap<String, HashMap<Product, Double>> warehouseMap = new HashMap<String, HashMap<Product, Double>>();
        Product product = new Product("product1", new ProductDetailsDummy());
        warehouseMap.put(warehouse1, new HashMap<Product, Double>());
        warehouseMap.put(warehouse2, new HashMap<Product, Double>());
        warehouseMap.get(warehouse1).put(product, 50.0);
        warehouseMap.get(warehouse2).put(product, 20.0);
        wareHouse = new WarehouseTestImplementation(warehouseMap);
    }

    @Test
    public void testOrderIsFilledIfEnoughInWareHouse() {
        // setup
        Product product = new Product("product1", new ProductDetailsDummy());
        Order order = new Order(warehouse1, product, 50.0);

        // exercise
        order.fill(wareHouse);

        // verify
        assertTrue(order.isFilled());

        assertEquals("Remaining quantity doesnt match", 0.0, wareHouse.getQuantity(warehouse1, product), epsilon);
    }

    @Test
    public void testFilingDoesNotRemoveIfNotEnoughInWareHouse() {
        // setup
        Product product = new Product("product1", new ProductDetailsDummy());
        Order order = new Order(warehouse2, product, 50.0);

        // exercise
        order.fill(wareHouse);

        // verify
        assertFalse(order.isFilled());
        assertEquals("Remaining quantity doesnt match", 20.0, wareHouse.getQuantity(warehouse2, product), epsilon);
    }
}






















