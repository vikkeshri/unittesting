package exampleOrder;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by vivek.ke
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderInteractionTester {
    @Mock
    private WareHouse wareHouseMock;

    @Test
    public void testOrderIsFilledIfEnoughInWareHouse() {
        String warehouse1 = "warehouse1";
        // setup
        Product product = new Product("product1", new ProductDetailsDummy());
        Order order = new Order(warehouse1, product, 50.0);
        when(wareHouseMock.hasQuantity(warehouse1, product, 50.0)).thenReturn(true);
        when(wareHouseMock.remove(warehouse1, product, 50.0)).thenReturn(true);
        InOrder inOrder = inOrder(wareHouseMock);


        // exercise
        order.fill(wareHouseMock);

        // verify
        inOrder.verify(wareHouseMock).hasQuantity(warehouse1, product, 50.0);
        inOrder.verify(wareHouseMock).remove(warehouse1, product, 50.0);
        assertTrue(order.isFilled());

        verifyNoMoreInteractions(wareHouseMock);
    }

    @Test
    public void testFilingDoesNotRemoveIfNotEnoughInWareHouse() {
        String warehouse2 = "warehouse2";
        // setup
        Product product = new Product("product1", new ProductDetailsDummy());
        Order order = new Order(warehouse2, product, 50.0);
//        when(wareHouseMock.hasQuantity(warehouse2, 50)).thenReturn(false);

        // exercise
        order.fill(wareHouseMock);

        // verify
        verify(wareHouseMock, times(1)).hasQuantity(warehouse2, product, 50.0);
        verify(wareHouseMock, never()).remove(warehouse2, product, 50.0);
        assertFalse(order.isFilled());
        verifyNoMoreInteractions(wareHouseMock);
    }
}
