package exampleOrder;

import java.util.HashMap;

/**
 * Created by vivek.ke.
 */
public class WarehouseTestImplementation implements WareHouse {
    private HashMap<String, HashMap<Product, Double>> warehouseQuantities;

    public WarehouseTestImplementation(HashMap<String, HashMap<Product, Double>> warehouseQuantities) {
        this.warehouseQuantities = warehouseQuantities;
    }

    public boolean hasQuantity(String warehouse, Product product, Double quantity) {
        return (warehouseQuantities.containsKey(warehouse) &&
                warehouseQuantities.get(warehouse).containsKey(product) &&
                warehouseQuantities.get(warehouse).get(product) >= quantity);
    }


    public boolean remove(String warehouse, Product product, Double quantity) {
        if (warehouseQuantities.containsKey(warehouse) &&
                warehouseQuantities.get(warehouse).containsKey(product) &&
                warehouseQuantities.get(warehouse).get(product) >= quantity) {
            warehouseQuantities.get(warehouse).put(product, warehouseQuantities.get(warehouse).get(product) - quantity);
            return true;
        } else
            return false;
    }


    public Double getQuantity(String warehouse, Product product) {
        if (warehouseQuantities.containsKey(warehouse) && warehouseQuantities.get(warehouse).containsKey(product))
            return warehouseQuantities.get(warehouse).get(product);
        else
            return 0.0;
    }
}
