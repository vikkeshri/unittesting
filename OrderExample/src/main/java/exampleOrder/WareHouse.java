package exampleOrder;

/**
 * Created by vivek.ke
 */
public interface WareHouse {
    public boolean hasQuantity(String warehouse, Product product, Double quantity);

    public boolean remove(String warehouse, Product product, Double quantity);

    public Double getQuantity(String warehouse, Product product);
}
