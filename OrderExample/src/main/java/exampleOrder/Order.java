package exampleOrder;

/**
 * Created by vivek.ke.
 */
public class Order {
    private String warehouseName;
    private Product product;
    private Double quantity;
    private boolean filled;

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public Order(String warehouseName, Product product, Double quantity) {
        this.warehouseName = warehouseName;
        this.product = product;
        this.quantity = quantity;
    }

    public void fill(WareHouse wareHouse) {
        if (wareHouse.hasQuantity(warehouseName, product, quantity))
            setFilled(wareHouse.remove(warehouseName, product, quantity));
    }

}
