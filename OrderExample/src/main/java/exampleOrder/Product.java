package exampleOrder;

/**
 * Created by User: vivek.ke
 * Date: 7/23/15
 */
public class Product {
    private String productId;
    private ProductDetails productDetails;

    public Product(String productId, ProductDetails productDetails) {
        this.productId = productId;
        this.productDetails = productDetails;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public int hashCode() {
        return productId.hashCode();
    }

    public boolean equals(Object object) {
        if (object instanceof Product) {
            Product product = (Product) object;
            return getProductId().equals(product.getProductId());
        }
        return false;
    }
}
