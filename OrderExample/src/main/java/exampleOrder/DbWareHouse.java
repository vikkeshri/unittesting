package exampleOrder;

import java.sql.*;
import java.util.HashMap;


/**
 * Created by vivek.ke
 */
public class DbWareHouse implements WareHouse {

    public Connection getDBConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/unittest", "demo", "password");
        } catch (SQLException e) {
            System.out.println("Error: Unable to connect mysql server");
            e.printStackTrace();
        }
        return connection;
    }

    public HashMap<String, HashMap<Product, Double>> getInventory() {
        HashMap<String, HashMap<Product, Double>> warehouseQuantities = new HashMap<String, HashMap<Product, Double>>();
        try {
            Connection connection = getDBConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from warehouse");
            while (resultSet.next()) {
                String productName = resultSet.getString("product_name");
                String manufacturer = resultSet.getString("manufacturer_name");
                Double price = resultSet.getDouble("price");
                ProductDetails productDetails = new ProductDetails(productName, manufacturer, price);
                Product product = new Product(resultSet.getString("product_id"), productDetails);
                String warehouseName = resultSet.getString("warehouse_name");
                Double quantities = resultSet.getDouble("quantity");
                if (!warehouseQuantities.containsKey(warehouseName))
                    warehouseQuantities.put(warehouseName, new HashMap<Product, Double>());
                if (!warehouseQuantities.get(warehouseName).containsKey(product))
                    warehouseQuantities.get(warehouseName).put(product, quantities);
                else {
                    quantities += warehouseQuantities.get(warehouseName).get(product);
                    warehouseQuantities.get(warehouseName).put(product, quantities);
                }
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return warehouseQuantities;
    }

    public boolean hasQuantity(String warehouse, Product product, Double quantity) {
        HashMap<String, HashMap<Product, Double>> warehouseQuantities = getInventory();
        return ((warehouseQuantities.containsKey(warehouse) &&
                (warehouseQuantities.get(warehouse).containsKey(product)) &&
                (warehouseQuantities.get(warehouse).get(product) >= quantity)
        ));
    }

    public boolean remove(String warehouse, Product product, Double quantity) {
        HashMap<String, HashMap<Product, Double>> warehouseQuantities = getInventory();
        if (warehouseQuantities.containsKey(warehouse) &&
                warehouseQuantities.get(warehouse).containsKey(product) &&
                warehouseQuantities.get(warehouse).get(product) >= quantity) {
            Double earlierQuantity = warehouseQuantities.get(warehouse).get(product);
            warehouseQuantities.get(warehouse).put(product, earlierQuantity - quantity);
            updateInventory(warehouseQuantities);
            return true;
        } else
            return false;
    }

    public void updateInventory(HashMap<String, HashMap<Product, Double>> warehouseQuantities) {

        try {
            Connection connection = getDBConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("update warehouse set quantity = ? where warehouse_name = ? and product_id = ?");
            for (String warehouse : warehouseQuantities.keySet()) {
                for (Product product : warehouseQuantities.get(warehouse).keySet()) {
                    preparedStatement.setString(2, warehouse);
                    preparedStatement.setString(3, product.getProductId());
                    preparedStatement.setDouble(1, warehouseQuantities.get(warehouse).get(product));
                    preparedStatement.executeUpdate();
                }
            }
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Double getQuantity(String warehouse, Product product) {
        HashMap<String, HashMap<Product, Double>> warehouseQuantities = getInventory();
        if (warehouseQuantities.containsKey(warehouse) && warehouseQuantities.get(warehouse).containsKey(product))
            return warehouseQuantities.get(warehouse).get(product);
        else
            return .0;
    }

}
