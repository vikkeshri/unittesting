package exampleOrder.Entities;

/**
 * Created by User: vivek.ke
 * Date: 7/23/15
 */
public class Product {
    private String productId;
    private exampleOrder.ProductDetails productDetails;

    public Product(String productId, exampleOrder.ProductDetails productDetails) {
        this.productId = productId;
        this.productDetails = productDetails;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public exampleOrder.ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(exampleOrder.ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public int hashCode() {
        return productId.hashCode();
    }

    public boolean equals(Object object) {
        if(object instanceof exampleOrder.Product) {
            exampleOrder.Product product = (exampleOrder.Product) object;
            return getProductId().equals(product.getProductId());
        }
        return false;
    }
}
