package exampleOrder.Entities;

import java.util.Date;

/**
 * Created by User: vivek.ke
 * Date: 7/23/15
 */
public class ProductDetails {
    private String productName;
    private String manufacturerName;
    private Double price;

    public ProductDetails(String productName, String manufacturerName, Double price) {
        this.productName = productName;
        this.manufacturerName = manufacturerName;
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
