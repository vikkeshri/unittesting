package sample;

import util.TextPreprocessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created inside IntelliJ IDEA.
 * User: kushagra.s
 * Date: 7/22/2015
 * Time: 4:02 PM
 */
public class ContentProcessor {

    public static List<String> getLines( String content ) {
        content = TextPreprocessor.convertNormalNGramBoundaries( content );
        content = TextPreprocessor.convertUnicodeNGramBoundaries( content );
        ArrayList<String> sentences = new ArrayList<>();
        for( String sentence: content.split( "\\. " ) )
            sentences.add( sentence.trim() );
        return sentences;
    }
}
