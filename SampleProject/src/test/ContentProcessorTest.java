import org.junit.Test;
import sample.ContentProcessor;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created inside IntelliJ IDEA.
 * User: kushagra.s
 * Date: 7/22/2015
 * Time: 4:15 PM
 */
public class ContentProcessorTest {

    @Test
    public void splitsContentIntoIndividualSentences() {
        //Arrange
        String content = "Retirement Plans At Every Employer! Is Your State Next? Paid sick leave, raising the minimum wage, commuter benefits. What’s next on the list of items employers are going to have to offer their employees?";

        //Act
        List<String> contentSentences = ContentProcessor.getLines( content );

        //Assert
        List<String> assertSentences = new ArrayList<String>();
        assertSentences.add( "Retirement Plans At Every Employer" );
        assertSentences.add( "Is Your State Next" );
        assertSentences.add( "Paid sick leave" );
        assertSentences.add( "raising the minimum wage" );
        assertSentences.add( "commuter benefits" );
        assertSentences.add( "What’s next on the list of items employers are going to have to offer their employees" );
        for( int i = 0; i < assertSentences.size(); i++ )
            assertThat( "Error in Processing Content:", contentSentences.get( i ), equalTo( assertSentences.get( i)));
    }
}
